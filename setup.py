import os
from setuptools import setup

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "BeckSoft",
    version = "0.1.0",
    author = "Kevin Pinto",
    author_email = "kevinganhito@gmail.com",
    description = ("An easy interface to interact with beckhoff modules."),
    license = "BSD",
    keywords = "Beckhoff EtherCAT",
    url = "https://github.com/Ultimaker/BeckSoft.git",
    packages=['beckSoft'],
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: BSD License",
    ],
)
