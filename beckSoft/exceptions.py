'''
Created on May 3, 2018

@author: Kevin Filipe, Ultimaker, k.ganhitopinto@ultimaker.com or kevinganhito@gmail.com
'''

class Error(Exception):
    '''
        Base Class for other exceptions
    '''
    pass

class ValueTooSmallError(Error):
    '''
        Raised when provided value is too small
    '''
    pass

class ValueTooLargeError(Error):
    '''
        Raised when provided value os too big
    '''
    pass

class NoFilesInDirError(Error):
    '''
        Raised when there are no files in directory
    '''
    pass

class NonExistentConfigFile(Error):
    '''
        Raised when there is no file for the wanted configuration
    '''
    pass

class AccessDeniedError(Error):
    '''
        Raised when the access to the parameter dictionary is not allowed
    '''

class AccessDeniedInOpState(Error):
    '''
        Raised when the access to the parameter dictionary is not allowed in Operational state
    '''
    
class AccessDeniedInUnknownState(Error):
    '''
        Raised when the access to the parameter dictinoary is requested but there is a unknown state coming from Beckhoff configuration XML file.
    '''
class UnknownError(Error):
    '''
        Raised only for debugging purposes about a unknown error. 
    '''
class UnknownDataTypeError(Error):
    '''
        Raised when an unknown datatype was received
    '''
    




        