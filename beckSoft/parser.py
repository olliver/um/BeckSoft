'''
Created on Apr 30, 2018

@author: Kevin Filipe, Ultimaker, k.ganhitopinto@ultimaker.com or kevinganhito@gmail.com
'''
from os import listdir
import os
from os.path import isfile, join
import pickle
import sys

import xmltodict

try:
    from exceptions import ValueTooLargeError, NoFilesInDirError, NonExistentConfigFile, UnknownError, ValueTooSmallError, ValueTooLargeError
except:
    from beckSoft.exceptions import ValueTooLargeError, NoFilesInDirError, NonExistentConfigFile, UnknownError, ValueTooSmallError, ValueTooLargeError

class Parser():
    '''
    classdocs
    '''    
    def __init__(self,dirPath="",alternativePath = None):
        '''
            Object Initialization
            :param dirPath: receives directory path string.
            :type: str
            
            :raises TypeError: if dirPath is not a string.
        '''
        
        # Defensive Programming - Checking pre-conditions
        if not isinstance(dirPath, str):
            raise TypeError("Argument type error. Was expecting <class 'str'>.")
        
        self.alternativePath = alternativePath
        if not isinstance(alternativePath, str):
            if alternativePath != None:
                raise TypeError("Argument type error. Was expecting <class 'str'> or <class 'None'>.")
        else: 
            # Check if alternative path directory has a directory or if it is necessary to make one
            if not os.path.exists(alternativePath):
                os.makedirs(alternativePath)    
            if isinstance(alternativePath, str):
                self.alterFiles = [f for f in listdir(alternativePath) if isfile(join(self.alternativePath, f))]
        print("[Parser] __init__ - Setting directory path. Path: {:s}".format(dirPath))
        self.dirPath = dirPath
        self.onlyfiles = [f for f in listdir(self.dirPath) if isfile(join(self.dirPath, f))]   
        if self.onlyfiles == None:
            raise NoFilesInDirError("No Files in \"{}\" found".format(self.dirPath))        
        #print(self.onlyfiles)
        
        
    def findConfig(self, name, revision, subname='default', fn = ""):
        '''
            Searches for Beckhoff module on all XML files.
            
            :param name: Beckhoff module
            :type: str
            
            :param revision: Beckhoff module revision number 
            :type: int
            
            :return: returns all respective Beckhoff module configuration data if found. Otherwise returns None
            :rtype: dict
            
            :raises TypeError: If provided name is not a string. /
                               If provided revision is not a int. /
                               If XML to Dict does not provide a dictionary. /
            :raises ValueTooLargeError: If provided revision number is bigger than 32 bits.
            :raises NonExistentConfigFile: If no configuration file was found in XML file.
            :raises KeyError: If XML structure does not correspond to this parsing function.
        '''
        
        # Defensive Programming arguments type check
        if not isinstance(name, str):
            raise TypeError("Argument type error. Was expecting <class 'str'>.")
        if not isinstance(revision, int):
            raise TypeError("Argument type error. Expecting <class 'int'>.")
        if (revision > int("FFFFFFFF",16)):
            raise ValueTooLargeError('Value is too large. Maximum size allowed is FFFFFFFF.')
        
        dev = None
        if isinstance(self.alternativePath, str):
            fileName = name + '_' + '{:08x}'.format(revision) + '_' + subname + '.bckhoff'
            if os.path.exists(self.alternativePath + fileName):
                #print("[Parser] findConfig() - Configuration Found for Name: {:s} - RevNo: {:08x}".format(name,revision))    
                return self.__loadConfig(self.alternativePath + fileName)
            else:
                pass
                #print('Debug Filename: ', fileName)
                #print('[Parser] findConfig() - No configuration file in fast config load path. Continuing to XML file')
        # Get filename for the correspondent beckhoff module
        if fn == "":
            fileName = self.__createFileName(name)
        else:
            fileName = fn
        # Check if fileName is not empty
        if fileName == None:
            raise NonExistentConfigFile('[Parser] findConfig() - Configuration file does not exist.')
        # Convert XML file into dictionary 
        with open(self.dirPath+fileName,'r',encoding='latin-1') as fd:
            doc = xmltodict.parse(fd.read())
        
        if not isinstance(doc, dict):
            raise TypeError("XML to Dict did not provide a dictionary.")    
         
        # Check if everything is fine with the provided XML file.    
        if ('EtherCATInfo' in doc):
            if('Descriptions' in doc['EtherCATInfo']):
                if('Devices' in doc["EtherCATInfo"]["Descriptions"]):
                    if('Device' in doc["EtherCATInfo"]["Descriptions"]["Devices"]):
                        pass
                        #print('[Parser] findConfig() - Devices found in configuration file.')
                    else:
                        raise KeyError("Dictionary key \"Device\" not found.")
                else:
                    raise KeyError("Dictionary key \"Devices\" not found.")
            else:
                raise KeyError('Dictionary key \"Descriptions\" not found.')          
        else:
            raise KeyError("Dictionary key \"EtherCATInfo\" not found.")    
            
        device = doc["EtherCATInfo"]["Descriptions"]["Devices"]["Device"];
        
        strFlag = 0
        for dev in device:
            # Check if there is a Type in device configuration. If not XML might be misleading. Check for the file manually
            if(isinstance(dev,str)):
                dev = device
                if strFlag == 1:
                    break
                strFlag = 1
            if 'Type' in dev:
                if (isinstance(dev["Type"], dict)):
                    if "#text" in dev["Type"]:
                        if(dev["Type"]['#text'] == name ):
                            if '@RevisionNo' in dev["Type"]:
                                rev = int(dev["Type"]["@RevisionNo"].replace("#x","{:1s}".format("")),16)
                                if(rev == revision):
                                    print("[Parser] findConfig() - Configuration Found for Name: {:s} - RevNo: {:08x}".format(dev["Type"]['#text'],rev))    
                                    break
                                else:
                                    continue
                            else:
                                break
                    else:
                        raise KeyError("Dictionary key \"#Text\" for the device \"Type\" not found")

                elif dev["Type"] == name:
                    break
            else:
                raise KeyError("Dictionary key \"Type\" for the device not found")
        
        # Save config 
        fileName = name.split()[0] + '_' + '{:08x}'.format(revision) + '_' + subname + '.bckhoff'
        if isinstance(self.alternativePath, str):
            self.__saveConfig(dev, self.alternativePath + fileName)
            
        return dev
    
    def __createFileName(self,name):
        '''
            From provided beckhoff device name generates the possible file name in previously provided path.
            
            :params name: beckhoff device name
            :type: str
            
            :return: returns filename if available in the previously provided XML directory path. Otherwise returns None
            :rtype: str
            
            :raises TypeError: If beckhoff device name is not a string
        '''
        
        # Defensive programming - pre-condition check
        if not isinstance(name, str):
            raise TypeError("Argument type error. Was expecting <class 'str>.")
        
        for indx1 in range(len(name)):
            for fileName in self.onlyfiles:
                indx = fileName[:-4].find(name[:(len(name)-indx1)])
                if(indx>-1):
                    if(fileName[indx+(len(name)-indx1)]=='x'):
                        return fileName
        return None
    
    def createProfileDict(self,device):
        '''
            Reads from de device configuration dictionary its profile. Its contents are "Parameters" and "DataType"
            
            :param device: Beckhoff device configuration dictionary
            :type: dict
            
            :return: returns Profile dictionary if found, otherwise returns None
            :rtype: dict
        '''
        parameter = {}
        dataType = {}
        # Parse device profile parameters
        if 'Profile' in device:
            if 'Dictionary' in device["Profile"]:
                if 'Objects' in device["Profile"]["Dictionary"]:
                    if 'Object' in device["Profile"]["Dictionary"]["Objects"]:
                        objects = device["Profile"]["Dictionary"]["Objects"]["Object"]
                        for obj in objects:
                            if 'Name' in obj:
                                parameter.update({obj['Name']:obj})      
                    else:
                        profile = None
                # Parse device profile dataTypes                
                if 'DataTypes' in device['Profile'] ["Dictionary"]:
                    for dtypes in device['Profile'] ["Dictionary"]['DataTypes']['DataType']:
                        dataType.update({dtypes['Name']:dtypes})
                                
                profile = {'DataType': dataType, 'Parameter':parameter}
            else:
                profile = None
        else:
            profile = None
        
        return profile

    def read_parameter(self,profile,paramName,subParamName="SubIndex 000",value=""):
        index = 0
        subindex = 0
        bitSize = 0      
        # Defensive programming
            
        if not isinstance(paramName, str) and not isinstance(subParamName, str):
            raise TypeError('Argument type error. Was expecting <class \'str\'.>')
            
        if not ('Parameter' in profile and paramName in profile['Parameter']):
            raise KeyError('Required keys in profile dictionary not found.')
       
        # Function body
        parameter = profile['Parameter'][paramName]
        
        if 'Index' in parameter:
            index = parameter['Index']
        else:
            raise UnknownError('There should be always an Index key. Please check XML file')
        
        if 'Info' in parameter:
            if 'DefaultData' in parameter['Info']:
                value = self.process_param_data(parameter['Info'],value)
                subindex, dataType, bitSize, flags = self.process_param_dataType(profile,parameter['Type'], subParamName)
                if 'Flags' in parameter:
                    flags = parameter['Flags']
                else:
                    raise UnknownError('Flags key not found and must be found. Please debug.')
                
            elif 'SubItem' in parameter['Info']:
                subNameFoundFlag= False
                for subItem in parameter['Info']['SubItem']:
                    if isinstance(subItem, str):
                        subItem = parameter['Info']['SubItem']

                    if 'Name' in subItem:
                        if subParamName == subItem['Name']:
                            subNameFoundFlag = 'True'
                            # Process the parameter data
                            if 'Info' in subItem:
                                value = self.process_param_data(subItem['Info'],value)
                            else:
                                UnknownError("There should be a Info key at this dictionary.")
                            
                            # Process dataType
                            subindex, dataType, bitSize, flags = self.process_param_dataType(profile,parameter['Type'], subParamName)
                            if flags == None:
                                if 'Flags' in parameter:
                                    flags = parameter['Flags']
                                else:
                                    raise UnknownError('Flags key not found and must be found. Please debug.')
                            break
                        else:
                            continue
                    else:
                        UnknownError("There should be at least a sub name in the subItem entry.")
                
                if subNameFoundFlag == False: ## means no subParameter name was found in the subItems. No default value and its actually not needed
                    subindex, dataType, bitSize, flags = self.process_param_dataType(profile,parameter['Type'], subParamName)
                    value = self.process_param_data(subItem['Info'],value)
                    if flags == None:
                        if 'Flags' in parameter:
                            flags = parameter['Flags']
                        else:
                            raise UnknownError('Flags key not found and must be found. Please debug.')
            else:
                subindex, dataType, bitSize, flags = self.process_param_dataType(profile,parameter['Type'], subParamName)
                value = self.process_param_data(subItem['Info'],value)
                if flags == None:
                    if 'Flags' in parameter:
                        flags = parameter['Flags']
                    else:
                        raise UnknownError('Flags key not found and must be found. Please debug.')
                #raise UnknownError("There should be at leas one a DefaultData or SubItem entry in the dictionary.")    
              
        else:
            value = self.process_param_data(parameter,value)
            subindex, dataType, bitSize, flags = self.process_param_dataType(profile,parameter['Type'], subParamName)
            if 'Flags' in parameter:
                flags = parameter['Flags']
            else:
                raise UnknownError('Flags key not found and must be found. Please debug.')
            #raise UnknownError("There should be at leas one a DefaultData or SubItem entry in the dictionary.")    
            #raise UnknownError("There should be a Info entry in dictionary")
    
        # Pad zeros to vlaue to keep it pretty in case its irrelevant
        if value == '':
            value = value.zfill(2*(1*(int(bitSize,10)%8 != 0)+int(int(bitSize,10)/8)))

        return int(index.replace('#x',""),16), int(subindex,10), dataType, int(bitSize,10), flags, value

    def process_param_data(self,infoParam, value):
        if value == "":
            if 'DefaultData' in infoParam:
                value = infoParam['DefaultData']
                value = "".join(map(str.__add__, value[-2::-2] ,value[-1::-2]))
            elif 'DefaultValue' in infoParam:
                value = infoParam['DefaultValue']
                if value.startswith('#x'):
                    if len(value)%2 != 0:
                        value = str(int(value.replace('#x',"0"),16))
                    else:
                        value = str(int(value.replace('#x',""),16))
                else:
                    UnknownError("Value is not starting with #x. Cannot convert") 
        else:
            if 'MinValue' in infoParam and 'MaxValue' in infoParam:
                minValue = int(infoParam['MinValue'].replace('#x',""),16)
                maxValue = int(infoParam['MaxValue'].replace('#x',""),16)
                if  minValue > int(value,10):
                    raise ValueTooSmallError('Provided value is too small.')
                if  maxValue  < int(value,10):
                    raise ValueTooLargeError('Provided value is too large.')                        
        return value

    def process_param_dataType(self,profile,dataType,subParamName):
        subIndex = None
        bitSize = None
        flags = None
        datype = None
        
        if dataType in profile['DataType']:
            dType = profile['DataType'][dataType]
            if 'SubItem' in dType:
                for subItem in dType['SubItem']:
                    if 'Name' in subItem:
                        if subParamName == subItem['Name']:
                            if 'SubIdx' in subItem:
                                subIndex = subItem['SubIdx']
                                bitSize = subItem['BitSize']
                                datype = subItem['Type']
                                if 'Flags' in subItem:
                                    flags = subItem['Flags']
                                else:
                                    KeyError("No Flags key found. Please check XML file manually.")
                                break
                        elif 'Elements' == subItem['Name']:
                            if subParamName.startswith('SubIndex'):
                                subIndex = str(int(subParamName.replace('SubIndex ',""),10))        
                            else:
                                UnknownError("Do not know how to get subIndex.")
                                
                            if 'Flags' in subItem:
                                flags = subItem['Flags']
                            else:
                                KeyError("No Flags key found. Please check XML file manually.")
                            
                            arrayType = profile['DataType'][subItem['Type']]
                            if 'BaseType' in arrayType:
                                baseType = profile['DataType'][arrayType['BaseType']]
                                if 'BitSize' in baseType:
                                    bitSize = baseType['BitSize']
                                else:
                                    raise KeyError('BitSize Key in BaseType not found. Please check XML file')
                                if 'Name' in baseType:
                                    datype = baseType['Name']
                                else:
                                    raise KeyError('Type Key in BaseType not found. Please check XML file.')
                            else:
                                raise KeyError('BaseType key not found. Please check XML file manually.')
            else:
                if 'Name' in dType:
                    datype = dType['Name']
                else:
                    raise UnknownError("Possible XML Name key missing. Check if has Name entry.")
                
                if 'BitSize' in dType:
                    bitSize = dType['BitSize']
                else:
                    raise UnknownError("Possible XML Name key missing. Check if has Name entry.")
                subIndex = '0'
                flags = None
        
        if (subIndex == None) and (bitSize == None) and (flags == None) and (datype == None):
            raise UnknownError("Invalid data to return. Possible reason can be the a wrong assignment of sub parameter name.")
        
        return subIndex, datype, bitSize, flags

    def read_param_permission(self, flag, state):
        if 'Access' in flag:
            access = ''
            if isinstance(flag['Access'], str):
                access = flag['Access']
            elif isinstance(flag['Access'], dict):
                if '#text' in flag['Access']:
                    access = flag['Access']['#text']
                    if '@WriteRestrictions' in flag['Access']:
                        if state != flag['Access']['@WriteRestrictions']:
                            raise UnknownError('Permission denied. You cannot use this parameter in this state.')
                else:
                    raise UnknownError('Cannont parse this part of the XML file.')
        else:
            raise UnknownError("Flag should always have a \'Access\' key.")
        return access

    # Fast loading
    # def loadPreviousConfig(self,name):
    
    def __saveConfig(self,configDict,pf):
        if self.alternativePath != None:
            pickle_out = open(pf,"wb")
            pickle.dump(configDict, pickle_out)
        
    def __loadConfig(self,pf):
        if self.alternativePath != None:
            pickle_in = open(pf,"rb")
            return pickle.load(pickle_in)
    
    def __save_Mapping(self,item,bitCounter,subMappings):
        if 'Entry' in item:
            subMappings.update({'BitStart':str(bitCounter)})
            for entry in item['Entry']:
                if isinstance(entry, str): # little hack if the entry has only one element
                    entry = item['Entry']
                    if 'BitLen' in entry:
                        if 'Name' in entry:
                            entry.update({'BitStart':str(bitCounter)})
                            bitCounter += int(entry['BitLen'],10)
                            subMappings.update({entry['Name']:entry})
                        else:
                            raise UnknownError('Some error happened. Please report to developer. Possible reason is XML file configuration.')   
                    else:
                        raise UnknownError('Some error happened. Please report to developer. Possible reason is XML file configuration.')
                    break                                 
            
                if 'BitLen' in entry:
                    if 'Name' not in entry:
                        entry.update({'Name':'Not Assigned'})
                        print('Name not found. Assigning -Not Assigned-')   
                    entry.update({'BitStart':str(bitCounter)})
                    bitCounter += int(entry['BitLen'],10)
                    subMappings.update({entry['Name']:entry})
                else:
                    raise UnknownError('Some error happened. Please report to developer. Possible reason is XML file configuration.')   
            subMappings.update({'BitLen':str(bitCounter-int(subMappings['BitStart'],10))})    
        else:
            raise UnknownError('It should have at least one error.')

        return bitCounter, subMappings
        
    def read_Mapping(self, device, pdo, mode):
        '''
            Inputs mapping idea: Put input port Name. With bit start and size it is possible to mask values and obtain data from raw data buffer
            This process is only to create a easy way to read the values
        '''
        mapping = {}
        subMappings = {}     
        bitCounter = 0
        strFlag = 0      
        if pdo in device:
            for item in device[pdo]:                
                # if this condition happens then it means there is only one PDO entry in the device
                if isinstance(item, str): 
                    item = device[pdo]
                    if strFlag == 1:
                        break
                    strFlag = 1
                tmpVar = item 
                hasSM = '@Sm' in item
                isMandatory = '@Mandatory' in item
                hasSM_mode = mode =='3' or mode == '2' or mode =='0' or mode == '1'

                if isMandatory:
                    bitCounter, subMappings = self.__save_Mapping(item,bitCounter,subMappings)
                    if isinstance(tmpVar,str):
                        mapping.update({item['Name']:subMappings})
                        subMappings = { }
                        break
                else:
                    if hasSM_mode:
                        if hasSM:
                            if item['@Sm'] == mode:
                                bitCounter, subMappings = self.__save_Mapping(item,bitCounter,subMappings)
                        else:
                            continue
                    else: # happens when there is no SM mode and so it selects the 'Fixed' from XML file
                        bitCounter, subMappings = self.__save_Mapping(item,bitCounter,subMappings)
                if len(subMappings.keys()) != 0:
                    mapping.update({item['Name']:subMappings})
                    subMappings = {}
                
        return mapping
    
    def get_all_parameters(self,profile):
        if not isinstance(profile, dict):
            print('Invalid profile.')
            sys.exit(0)
    
        commands = []
    
        if not 'Parameter' in profile:
            print('Invalid parameter')
            return []
        cmd1 = ''
        cmd2 = ''
        keys = profile['Parameter'].keys()
        for k in keys:
            param = profile['Parameter'][k]
            if not isinstance(param, dict):
                print('Should be a dict.')
                sys.exit(0)
            if 'Name' in param:
                cmd1 = param['Name']
            if 'Info' in param:
                if not 'SubItem' in param['Info']:
                    cmd2 = ""
                    commands.append([cmd1,cmd2])
                    continue
                if not isinstance(param['Info']['SubItem'],list):
                    sub = param['Info']['SubItem']
                    if 'Name' in sub:
                        if sub['Name'] != cmd1:
                            cmd2 = sub['Name']  
                        else:
                            cmd2 = ''
                    else:
                        print('Error')     
                    commands.append([cmd1,cmd2])
                    continue
                else:
                    for sub in param['Info']['SubItem']:
                        if 'Name' in sub:
                            if sub['Name'] != cmd1:
                                cmd2 = sub['Name']  
                            else:
                                cmd2 = ''
                        else:
                            print('Error')     
                        commands.append([cmd1,cmd2])
            else:
                cmd2 = ""
                commands.append([cmd1,cmd2])
                continue
        return commands  
    
if __name__ == "__main__":
    
    #moduleName = "EL7332"
    #moduleRev = int("00180000",16)
    
    moduleName = "FB1111 Dig. In/Out"
    moduleRev = int("0064008E",16)
    
    xmlConfigPath = "../BeckXML"
    dicConfigPath = "BeckXML/PyMotorConfig/"
    
    bp = Parser(xmlConfigPath,dicConfigPath)
    
    #device = bp.findConfig(moduleName,moduleRev)
    device = bp.findConfig(moduleName,moduleRev)
    
    if(device == None):
        print("Device not found in the possible configuration file")
        sys.exit(0)

    profile = bp.createProfileDict(device)
    if profile == None:
        print("Beckhoff module does not has any profile configuration")
        sys.exit(0)
    
    # Defining PreOP state
    state = "PreOP"
        
    print("Testing parameters and sub-parameters")
    print("| {:30s} | {:30s} | {:14s} | {:10s} | {:7s} | {:5s} | {:10s} |".format("Parameter Name",'Sub Name','Index:SubIndex','Type','BitSize','Flags','Value'))
    
    # Testing if setting value is working properly - Get too big value error                      
    cmd = ('SM output parameter','Sync mode')
    index, subindex, dataType, bitSize, flags, value = bp.read_parameter(profile, cmd[0], cmd[1],'4000')
    print("| {:30s} | {:30s} | {:6s}:{:7d} | {:10s} | {:7d} | {:5s} | {:10s} |".format(cmd[0],cmd[1],hex(index),subindex,dataType,bitSize,bp.read_param_permission(flags, state),value))
                                    
    
    # Testing if sub parameter is working - 1
    cmd = 'SM input parameter'
    index, subindex, dataType, bitSize, flags, value = bp.read_parameter(profile, cmd)
    print("| {:30s} | {:30s} | {:6s}:{:7d} | {:10s} | {:7d} | {:5s} | {:10s} |".format(cmd,"",hex(index),subindex,dataType,bitSize,bp.read_param_permission(flags, state),value))
    
    # Testing if sub parameter is working - 1
    cmd = ('SM input parameter', "Sync mode")
    index, subindex, dataType, bitSize, flags, value = bp.read_parameter(profile, cmd[0], cmd[1], "")
    print("| {:30s} | {:30s} | {:6s}:{:7d} | {:10s} | {:7d} | {:5s} | {:10s} |".format(cmd[0],cmd[1],hex(index),subindex,dataType,bitSize,bp.read_param_permission(flags, state),value))
    
    # Testing if sub parameter is working - 2
    cmd = ('SM input parameter', "Cycle time")
    index, subindex, dataType, bitSize, flags, value = bp.read_parameter(profile, cmd[0], cmd[1], "")
    print("| {:30s} | {:30s} | {:6s}:{:7d} | {:10s} | {:7d} | {:5s} | {:10s} |".format(cmd[0],cmd[1],hex(index),subindex,dataType,bitSize,bp.read_param_permission(flags, state),value))
   
    # Testing if sub parameter is working  and also hidden sub parameters that only are showen at the DataType and not in profile parameter - 3
    cmd = ('SM input parameter', "SM event missed counter")
    index, subindex, dataType, bitSize, flags, value = bp.read_parameter(profile, cmd[0], cmd[1], "")
    print("| {:30s} | {:30s} | {:6s}:{:7d} | {:10s} | {:7d} | {:5s} | {:10s} |".format(cmd[0],cmd[1],hex(index),subindex,dataType,bitSize,bp.read_param_permission(flags, state),value))

    # Testing if sub parameter is working  and also hidden sub parameters that only are showen at the DataType and not in profile parameter - 4
    cmd = 'Module list'
    index, subindex, dataType, bitSize, flags, value = bp.read_parameter(profile, cmd)
    print("| {:30s} | {:30s} | {:6s}:{:7d} | {:10s} | {:7d} | {:5s} | {:10s} |".format(cmd[0],"",hex(index),subindex,dataType,bitSize,bp.read_param_permission(flags, state),value))

    # Testing if sub parameter is working - 5
    cmd = ('Module list','SubIndex 001')
    index, subindex, dataType, bitSize, flags, value = bp.read_parameter(profile, cmd[0], cmd[1])
    print("| {:30s} | {:30s} | {:6s}:{:7d} | {:10s} | {:7d} | {:5s} | {:10s} |".format(cmd[0],cmd[1],hex(index),subindex,dataType,bitSize,bp.read_param_permission(flags, state),value))

    # Testing if sub parameter is working - 6
    cmd = ('Module list','SubIndex 002')
    index, subindex, dataType, bitSize, flags, value = bp.read_parameter(profile, cmd[0], cmd[1])
    print("| {:30s} | {:30s} | {:6s}:{:7d} | {:10s} | {:7d} | {:5s} | {:10s} |".format(cmd[0],cmd[1],hex(index),subindex,dataType,bitSize,bp.read_param_permission(flags, state),value))
    
    # Testing if sub parameter is working - 7
    cmd = ('Module list','SubIndex 003')
    index, subindex, dataType, bitSize, flags, value = bp.read_parameter(profile, cmd[0], cmd[1])
    print("| {:30s} | {:30s} | {:6s}:{:7d} | {:10s} | {:7d} | {:5s} | {:10s} |".format(cmd[0],cmd[1],hex(index),subindex,dataType,bitSize,bp.read_param_permission(flags, state),value))
  
    # Testing if sub parameter is working - 8                                          
    cmd = ('Module list','SubIndex 004')
    index, subindex, dataType, bitSize, flags, value = bp.read_parameter(profile, cmd[0], cmd[1])
    print("| {:30s} | {:30s} | {:6s}:{:7d} | {:10s} | {:7d} | {:5s} | {:10s} |".format(cmd[0],cmd[1],hex(index),subindex,dataType,bitSize,bp.read_param_permission(flags, state),value))
 
    # Testing if setting DefaultValue instead DefaultData is working properly - 9                            
    cmd = ('DCM Motor Settings Ch.1','Maximal current')
    index, subindex, dataType, bitSize, flags, value = bp.read_parameter(profile, cmd[0], cmd[1])
    print("| {:30s} | {:30s} | {:6s}:{:7d} | {:10s} | {:7d} | {:5s} | {:10s} |".format(cmd[0],cmd[1],hex(index),subindex,dataType,bitSize,bp.read_param_permission(flags, state),value))

    # Testing if setting value is working properly                      
    cmd = ('DCM Motor Settings Ch.1','Maximal current')
    index, subindex, dataType, bitSize, flags, value = bp.read_parameter(profile, cmd[0], cmd[1],'900')
    print("| {:30s} | {:30s} | {:6s}:{:7d} | {:10s} | {:7d} | {:5s} | {:10s} |".format(cmd[0],cmd[1],hex(index),subindex,dataType,bitSize,bp.read_param_permission(flags, state),value))
                                        
    # Testing if setting value is working properly - Get too big value error                      
    cmd = ('DCM Motor Settings Ch.1','Maximal current')
    index, subindex, dataType, bitSize, flags, value = bp.read_parameter(profile, cmd[0], cmd[1],'4000')
    print("| {:30s} | {:30s} | {:6s}:{:7d} | {:10s} | {:7d} | {:5s} | {:10s} |".format(cmd[0],cmd[1],hex(index),subindex,dataType,bitSize,bp.read_param_permission(flags, state),value))
    
    # Testing if setting value is working properly - Get too big value error                      
    cmd = ('Restore default parameters')
    index, subindex, dataType, bitSize, flags, value = bp.read_parameter(profile, cmd)
    print("| {:30s} | {:30s} | {:6s}:{:7d} | {:10s} | {:7d} | {:5s} | {:10s} |".format(cmd,'',hex(index),subindex,dataType,bitSize,bp.read_param_permission(flags, state),value))

    cmd = ('Restore default parameters', 'SubIndex 001')
    index, subindex, dataType, bitSize, flags, value = bp.read_parameter(profile, cmd[0], cmd[1], '#x64616F6C')
    print("| {:30s} | {:30s} | {:6s}:{:7d} | {:10s} | {:7d} | {:5s} | {:10s} |".format(cmd[0],cmd[1],hex(index),subindex,dataType,bitSize,bp.read_param_permission(flags, state),value))
                                                                           
    print("Parsing test example finished")

    
    
