'''
Created on Apr 19, 2018

@author: Kevin Filipe, Ultimaker, k.ganhitopinto@ultimaker.com or kevinganhito@gmail.com
'''

import binascii
import ctypes

import logging
import sys

from pysoem.pysoem import EthercatSlaveStates
from pysoem.pysoem import SOEM

try:
    from parser import Parser as BeckhoffParser
except:
    from beckSoft.parser import Parser as BeckhoffParser

try:
    from exceptions import AccessDeniedError, AccessDeniedInOpState, \
    AccessDeniedInUnknownState, UnknownDataTypeError, UnknownError
except:
    from beckSoft.exceptions import AccessDeniedError, AccessDeniedInOpState, \
    AccessDeniedInUnknownState, UnknownDataTypeError, UnknownError

#from beckhoff import Beckhoff


class Access:
    ro = 'ro'
    rw = 'rw'

# from beckhoff.Database import Beckhoff_Database


class Beckhoff:
    '''
    classdocs
    '''
    __beckSize = 0  # static variable to know how many Beckhoff devices are connected
    __initializedModules = {}
   
    __memory_counter = [0] * 113  # static variable used to calculate the offset for the buffer data    
    __data = []
    __timestamp = []
    __soem = []
    
    # Store or read data from file
    __io_map_path = None
    __io_map_file = None
    __mode = 'Default'
    __file_devices = None
    __hasData = True
    # Logging    
    __logger = None
    
    __bp = None
        
    def __init__(self, name: str, index: int) -> None:
        '''
        Construct a new Beckhoff object.
        
        :param name: Beckhoff ethercat module name.
        :type name: str.
        
        :param index: Beckhoff ethercat module position
        :type index: int
        
        :return: return nothing
        '''
        
        self.name = name
        self.index = index    
        Beckhoff.__beckSize += 1
        self.soem = Beckhoff.__soem        
        self.inputProp = {}
        self.outputProp = {}
        Beckhoff.__logger.info("{:03d} : {:s} - Initializing expected beckhoff module.".format(index,name))
        
    @staticmethod
    def reset():
        Beckhoff.__initializedModules = {}
       
        Beckhoff.__memory_counter = [0] * 113  # static variable used to calculate the offset for the buffer data    
        Beckhoff.__data = []
        Beckhoff.__timestamp = []
        Beckhoff.__soem = []
        
        # Store or read data from file
        Beckhoff.__io_map_path = None
        Beckhoff.__io_map_file = None
        Beckhoff.__mode = 'Default'
        Beckhoff.__file_devices = None
        Beckhoff.__hasData = True
        # Logging    
        Beckhoff.__logger = None
        
        Beckhoff.__bp = None       
    
    def isNamed(self, name: str) -> bool:
        '''
            Compares if the Beckhoff object has the provided name
            
            :param name: Name to compare
            :type: str
            
            :return: returns True if the module has the same name. Otherwise returns False
            :rtype: bool
            :raises TypeError: If provided name is not a string          
        '''        
        # Defensive Programming arguments type check
        if not isinstance(name, str):
            raise TypeError("Argument type error. Was expecting <class 'str'>.")
        return self.name == name
    
    def __automaticOffsetCalc(self, sourceProp : dict) -> None:
        '''
            Automatic Buffer Offset Calculation. Not computationally efficient, needs an update in a future version
            
            :param sourceProp: properties of incoming data (either input or output)
            
            :return: returns nothing. 
        ''' 
        
        # Computational non efficient function, because it creates a list and operates all over it. there should be a way to use less memory
        bitLen = sourceProp["BitLen"]
        if  range(sourceProp["start"] == sourceProp["end"]):
            offset = 0
        else:
            offset = Beckhoff.__memory_counter[sourceProp["start"]]  # Position of the bit of the byte pointed by inputProp["Start"]        

        for addrPos in range(sourceProp["start"], sourceProp["end"]):            
            if (Beckhoff.__memory_counter[addrPos] == 8):  # Check if it is already filled
                print("Error filling memory. Exiting...")
                sys.exit(0)
            elif(bitLen > 8):  # Check if it is less than zero
                Beckhoff.__memory_counter[addrPos] = 8
                bitLen -= 8
            else:
                Beckhoff.__memory_counter[addrPos] += bitLen
                bitLen = 0
              
            if (bitLen == 0) and (addrPos == sourceProp["end"]):  # Check if bitLen is 0 and addresPos is at self.inputProp["end"], otherwise map might be wrong
                print("Error bitLen doesnt end at the inputProp[\"end\". Exiting...")
                sys.exit(0)
                break;
                        
        return offset  
    
    def __mask(self,siz: int,offset: int, bitLen: int) -> int:
        '''
            Creates a mask by setting wanted bits to zero and all others to 1
            :param siz: mask size in bytes
            :type: int
            
            :param offset: start bit of the wanted bits. Counting is done from right to left
            :type: int
            
            :param bitLen: specifies how many bits are to be setted to zero.
            :type: int
            
            :return: returns the generated mask
            :rtype: int
            
        '''
        tmp = ((1 << (siz*8))-1)
        upper_mask = ((1 << (offset+bitLen))-1)
        lower_mask = ((1 << (offset)) - 1)
        return (tmp ^ upper_mask) | lower_mask
              
    def setOutput(self, data: str or int, cmd1: str = "", cmd2: str ="") -> None:
        '''
            Prepares the data to be sent by module.
            
            :param data: desired data to be put at the output address of the io_map
            :type: str or int
            
            :param cmd1: Command to specify which data the user wants
            :type: str
            
            :param cmd2: Sub command to specify a part of the selected data by the first command
            :type: str
            
            :return: returns nothing.
            :raises TypeError: If provided data is not a int or string
            :raises KeyErrpr: Provided key is not part of the dictionary
        '''
        # Setting data according to the provided names using cmd1 and cmd2
        
        if isinstance(data, str):
            if data.startswith("#x"):
                data = int(data[2:],16)
            else:
                data = int(data,10)
        elif isinstance(data, bytes):
            data = int.from_bytes(data,'big')
        elif isinstance(data,int):
            pass
        else:
            Beckhoff.__logger.error("Error handling provided data.")
            raise TypeError("Invalid type. Data should be either int or str.")  
       
        Beckhoff.__logger.debug("({:03d},{:s}) Setting \"{:s}\" \"{:s}\" with {:d}".format(self.index, self.name, cmd1,cmd2,data))

        # Check if it was provided a first command
        if cmd1 != "":
            # Checking if first command was mapped.            
            if not cmd1 in self.output_map:
                Beckhoff.__logger.error('Invalid command. Check syntax and if it exists.')
                raise KeyError('Invalid command. Check syntax and if it exists.')
            # Check if BitLen was declared in first command.
            if not 'BitLen' in self.output_map[cmd1]:
                Beckhoff.__logger.error('Error \"BitLen\" not found.')
                raise KeyError('Error BitLen not found')
            
            # Check if second command was provided by the user
            if cmd2 != "":
                if not cmd2 in self.output_map[cmd1]:
                    Beckhoff.__logger.error('Invalid command. Check syntax and if it exists.')
                    raise KeyError('Invalid command. Check syntax and if it exists')
                # Check if BitStart was declared in second command.
                if not 'BitStart' in self.output_map[cmd1][cmd2]:
                    Beckhoff.__logger.error('Error \"BitStart\" not found.')
                    raise KeyError('Error BitStart not found')
                bitStart = int(self.output_map[cmd1][cmd2]['BitStart'])
                bitLen  = int(self.output_map[cmd1][cmd2]['BitLen'])
                
                # Load mask for second command
                if "mask" in self.output_map[cmd1][cmd2]:
                    mask = self.output_map[cmd1][cmd2]['mask']
                else: 
                    mask = self.__mask(self.outputProp["size"],bitStart, bitLen)
                    self.output_map[cmd1][cmd2].update({"mask": mask})
            else:
                bitStart = int(self.output_map[cmd1]['BitStart'])
                bitLen  = int(self.output_map[cmd1]['BitLen'])
                # Load mask for first command
                if "mask" in self.output_map[cmd1]:
                    mask = self.output_map[cmd1]['mask']
                else: 
                    mask = self.__mask(self.outputProp["size"],bitStart, bitLen)
                    self.output_map[cmd1].update({"mask": mask})
            
            data = (data << bitStart)
            self.buffer_out = ((mask & int.from_bytes(self.buffer_out,'little')) | (data)).to_bytes(len(self.buffer_out),'little')                
            Beckhoff.__logger.debug('({:03d},{:s}) Mask (hex): {:s}'.format(self.index, self.name, binascii.hexlify(mask.to_bytes(len(self.buffer_out),'little')).decode('ascii')))
            Beckhoff.__logger.debug('({:03d},{:s}) Data (hex): #x{:s}'.format(self.index, self.name,binascii.hexlify(data.to_bytes(len(self.buffer_out),'little')).decode('ascii')))
            Beckhoff.__logger.debug('({:03d},{:s}) Output Buffer (hex): #x{:s}'.format(self.index, self.name,binascii.hexlify(self.buffer_out).decode('ascii')))
        else: # if u want to only fill the memory make sure you use its endiness too.
            self.buffer_out = (data).to_bytes(len(self.buffer_out),'little')
            Beckhoff.__logger.debug('({:03d},{:s}) Output Buffer (hex): #x{:s}'.format(self.index, self.name,binascii.hexlify(self.buffer_out).decode('ascii')))

    #@staticmethod
    def updateOutput(self) -> None:
        '''
            Writes data to be sent into the transmission buffer
            :return: returns nothing.
        '''
        self.soem.writeSend(self.buffer_out, self.outputProp["start"], self.outputProp["BitLen"], self.outputProp["bitStart"])

    def get_mask(self, config: dict) -> int:
        """
            Creates a bytes array to mask the data coming from input/output configuration
            
            :param config: Configuration dictionary to create a mask. Necessary dictionary entries are: ["start"],["end"],["size"],["bitStart"]
            :type: dict
            
            :return: returns created mask
            :type: int
            :raises TypeError: If provided name is not a dictionary          

        """
        
        if not isinstance(config, dict):
            raise TypeError("Argument type error. Was expecting <class 'dict'>.")
        tmp = int((1 << (config["BitLen"]) * config["size"]) - 1)
        return tmp
            
    def getInput(self, cmd1 = "", cmd2 = "") -> int:
        '''
            Gets module input data specified by the provided commands
            
            :param cmd1: Command to specify which data the user wants
            :type: str
            
            :param cmd2: Sub command to specify a part of the selected data by the first command
            :type: str
            
            :return: returns raw data from beckhoff module
            :rtype: int
            
            :raises KeyError: Provided commands are not part of the configuration dictionary.
            
        '''
        tmp = (int.from_bytes(Beckhoff.__data[self.inputProp["start"]:self.inputProp["end"]], 'big') >> self.inputProp["bitStart"] & self.inputProp["mask"]).to_bytes(self.inputProp["size"], 'big')
        if cmd1 != "":
            # Safety check for cmd1
            if cmd1 in self.inputs_map:
                bitLeng = int(self.inputs_map[cmd1]['BitLen'],10)
                bitStart = int(self.inputs_map[cmd1]['BitStart'],10)

                #Doing this because of the network endiness. if only 1 byte then the bit start is different. This could be optimized after a further study of some dif
                if self.inputProp["size"] != 1:
                    bitStart = self.inputProp["size"] * 8 - bitStart - bitLeng;
                tmp = (((1 << bitLeng)-1) & (int.from_bytes(tmp,'big') >> bitStart)).to_bytes((bitLeng%8!=0) + int(bitLeng/8),'little')
                if cmd2 != "":
                    # Safety check for cmd2
                    if cmd2 in self.inputs_map[cmd1]:
                        bitLeng = int(self.inputs_map[cmd1][cmd2]['BitLen'],10)
                        bitStart = int(self.inputs_map[cmd1][cmd2]['BitStart'],10)-int(self.inputs_map[cmd1]['BitStart'],10)
                        tmp = (((1 << bitLeng)-1) & (int.from_bytes(tmp,'big') >> bitStart)).to_bytes((bitLeng%8!=0) + int(bitLeng/8),'big')
                    else:
                        raise KeyError("Invalid sub command.")
            else:
                raise KeyError("Invalid command")
        return  tmp
    
    def setup(self, moduleConfig: dict, fn:str = 0) -> None:
        '''
            Setups Beckhoff modules with the provided configuration.
            
            :param: moduleConfig
            :type: dict
        
            :return: return nothing
            
            :raise TypeError: If argument has invalid type.
            :raise KeyError: If valid keys are not present in dictionary
            :raises UnknownError: If parser was never initialized. The reason is unknown.
        '''
        Beckhoff.__logger.info("({:03d}:{:6s}) Module setup.".format(self.index,self.name))
        if not isinstance(moduleConfig, dict):
            raise TypeError("Argument type error. Expecting <class 'dict'>.")
        
        # Get IOMapping start, end addresses as well as size
        if not ('revision' in moduleConfig) or not ('input' in moduleConfig) or not ('output' in moduleConfig):
            raise KeyError('moduleConfig does not contain valid keys.')
        
        self.revision = moduleConfig["revision"]
        self.inputProp.update(moduleConfig["input"])
        self.outputProp.update(moduleConfig["output"])
        Beckhoff.__logger.debug("({:03d}:{:6s}) IN(start: {:03d}, end: {:03d}, size: {:03d}, bitStart: {:01d}) OUT(start: {:03d}, end: {:03d}, size: {:03d}, bitStart: {:01d})".format(self.index,self.name,self.inputProp["start"], self.inputProp["end"], self.inputProp["size"], self.inputProp["bitStart"],self.outputProp["start"], self.outputProp["end"], self.outputProp["size"], self.outputProp["bitStart"])) 
        # Read Configuration File
        self.inputProp.update({"BitLen" :self.inputProp["bitLen"]})   
        self.outputProp.update({"BitLen" : self.outputProp["bitLen"]}) 

        # REMOVED AUTOMATIC OFFSET CALCULATION. SOEM was providing bitStart in the byte automatically, PySoem wasn't using that. 
        # offset = self.__automaticOffsetCalc(self.outputProp)
        # self.outputProp.update({"offset":offset})
        # offset = self.__automaticOffsetCalc(self.inputProp)
        # self.inputProp.update({"offset":offset})

        self.inputProp.update({"mask": self.get_mask(self.inputProp)})
        self.outputProp.update({"mask": self.get_mask(self.outputProp)})
        
        # # Reads Beckhoff XML Configuration File
        if(Beckhoff.__bp == None):
            Beckhoff.__logger.error("({:03d}:{:6s} Parser was never initialized".format(self.index,self.name))
            raise UnknownError('Parser was never initialized. Unknown reason.')
        self.configs = Beckhoff.__bp.findConfig(self.name, self.revision,fn=fn)
        if(self.configs == None):
            Beckhoff.__logger.error("({:03d}:{:6s}) Module configuration not found with Parser.".format(self.index,self.name))
        
        Beckhoff.__logger.info("({:03d}:{:6s}) Module profile creation".format(self.index,self.name))
        self.profile = Beckhoff.__bp.createProfileDict(self.configs)
        
        self.buffer_out = bytes(self.outputProp['size'])
                    
# Testing this module

    # def setParameter(self):

    @staticmethod
    def updateInput() -> None:
        '''
            Updates the received data to later be processed. This is done by transfering the data from a queue.
            :param: None
            :return: returns nothing
        '''
        if Beckhoff.__mode == 'Default':
            Beckhoff.__timestamp, Beckhoff.__data = Beckhoff.__soem.readData()  # Got timestamp and IOmap data
            if isinstance(Beckhoff.__io_map_path,str):
                Beckhoff.__io_map_file.write(str(Beckhoff.__timestamp) + '\t'+binascii.hexlify(Beckhoff.__data).decode('ascii') + '\t\n')
        if Beckhoff.__mode == 'ReadOnly':
                line = Beckhoff.__io_map_file.readline()
                if(line == ''):
                    return False
                substr = line.split('\t')
                Beckhoff.__timestamp =  int(substr[0])
                Beckhoff.__data = binascii.unhexlify(substr[1])
        return True
    
    @staticmethod
    def hasData() -> bool:
        '''
            Cheks if beckhoff modules have data to send or not
            :param: None
            :return: returns True or False depending if it has or not data to send.
        '''
        return Beckhoff.__hasData

    @staticmethod 
    def getTimeStamp() -> None:
        '''
            Reads the timestamp of the time when the actual data was received.
            :param: None
            :return: returns nothing
        '''
        return Beckhoff.__timestamp

    @staticmethod    
    def printOffsetAssignment(module: dict) -> None:
        '''
            Prints the Start, End address as well the bit start from the byte and also the data bit length of all declared devices.
            :param module: Beckhoff module strucured data
            :type: dict
            
            :return: returns nothing            
        '''  
        print("----------------------------------------------------------------------")
        print("| {:3s} | {:6s} | {:6s} | {:10s} | {:8s} | {:9s} | {:6s} |".format("Idx", "Name", "Type", "START ADDR", "END ADDR", "BIT_START", "BITLEN"))
        for device in Beckhoff.getModules():
            indx = device["index"] - 1
            print("| {:03d} | {:6s} | {:6s} | {:10d} | {:8d} | {:9d} | {:6d} |".format(device['index'],device['name'],"Output", module[indx].outputProp["start"], module[indx].outputProp["end"], module[indx].outputProp["bitStart"], module[indx].outputProp["BitLen"]))
            print("| {:03d} | {:6s} | {:6s} | {:10d} | {:8d} | {:9d} | {:6d} |".format(device['index'],device['name'],"Input", module[indx].inputProp["start"], module[indx].inputProp["end"], module[indx].inputProp["bitStart"], module[indx].inputProp["BitLen"]))
        print("----------------------------------------------------------------------")

    @staticmethod    
    def size() -> int:
        '''
            Get the quantity of all initialized beckhoff modules
            :params: None
            :return: quantity of initialized beckhoff modules
            :rtype: int
        '''
        return Beckhoff.__beckSize

    @staticmethod
    def dataSent() -> int:
        '''
            Check how many times user provided output data was sent.
            
            :param: None
            
            :return: returns how many times the output data was sent
            :rtype: int 
        '''
        return Beckhoff.__soem.dataSent()

    @staticmethod
    def start(freq=100)->None:
        '''
            Sets soem state to OPERATIONAL and starts capturing thread.
            
            :param freq: capturing thread operation frequency
            :type: int
            
            :return: returns nothing
            
            :raises TypeError: If provided freq is not a int          
        '''
        if Beckhoff.__io_map_file != None:
            if Beckhoff.__mode == 'Default':
                Beckhoff.__io_map_file= open(Beckhoff.__io_map_path,'a')
                Beckhoff.__io_map_file.write('--Data--\n')
            if Beckhoff.__mode == 'ReadOnly':
                Beckhoff.__io_map_file= open(Beckhoff.__io_map_path,'r')
                for line in Beckhoff.__io_map_file:
                    if line == '--Data--\n':
                        break

        if Beckhoff.__mode != 'ReadOnly':
            Beckhoff.__soem.setStateAutomatically(EthercatSlaveStates.EC_STATE_OPERATIONAL)
            Beckhoff.__soem.startCaptureThread(freq)
            Beckhoff.__logger.info("Capture thread started")
            if not isinstance(freq, int):
                raise TypeError("Argument type error. Was expecting <class 'int'>.")

    @staticmethod
    def stop()->None:
        '''
            Stops the capturing thread.
            :param: None
            
            :return: returns nothing
    
        '''
        if Beckhoff.__mode != 'ReadOnly':
            Beckhoff.__soem.stopCaptureThread()
            Beckhoff.__logger.info("Stopping the capture thread.")
        if Beckhoff.__io_map_path != None:
            Beckhoff.__io_map_file.close()

    @staticmethod
    def found(module: list)->bool:
        '''
            Checks if all provided modules are found in the user setup
            :param: modules
            :type: list
            
            :return: returns if all beckhoff modules are found at the user specified position
            :rtype: bool
        '''
        for device in Beckhoff.__soem.getDevices():
            if(not module[device["index"] - 1].isNamed(device["name"])):
                Beckhoff.__logger.error("Module not found {:6s} : {:x} at index {:2d}".format(device["name"], device["revision"], device["index"]))
                return False
            else:
                Beckhoff.__logger.info("Module found {:6s} : {:x} at index {:2d}".format(device["name"], device["revision"], device["index"]))
        return True
    
    @staticmethod
    def init(iface: str = "", configDir: str="", fastConfigDir: str="", logs : int = logging.INFO, logsDir: str = "../Logs", mode: str = 'Default') -> None:
        '''
            Initializes beckhoff modules ethernet interface
            :param iface: ethernet interface name. ex: eth0
            :type: str
            
            :param configDir: Beckhoff modules configuration directory. This directory should have XML files
            :type: str
            
            :param fastConfigDir: Beckhoff modules fast configuration directory. This module should have setup configuration dictionaries
            :type: str
          
            :param logs: Logging level
            :type: int   
                   
            :param logsDir: Logging directory
            :type: str
            
            :param mode: Beckhoff running mode. This can be either 'Default' or 'Read Only'. 'Read Only' mode is used to read Beckhoff txt data without being necessary having the test setup plugged in
            :type: str
            
            :return: returns nothing
        '''
        Beckhoff.reset()
        Beckhoff.__logger = logging.getLogger(__name__)
        hdlr = logging.FileHandler(logsDir + 'Beckhoff_Logs')
        formatter = logging.Formatter('%(asctime)s\t%(levelname)s  |  [Beckhoff]\t%(funcName)s()-\t%(message)s')
        hdlr.setFormatter(formatter)
        Beckhoff.__logger.addHandler(hdlr) 
        Beckhoff.__logger.setLevel(logs)    
        if mode == 'Default':
            Beckhoff.__logger.info('Logger created.')    
            Beckhoff.__soem = SOEM(iface)  # Sets SOEM
            Beckhoff.__logger.info('SOEM initialized at interface {:s}.'.format(iface))        
            Beckhoff.__bp = BeckhoffParser(configDir, fastConfigDir)
            Beckhoff.__logger.info('XML parser initialized. XML configuration directory: \"{:s}\". Fast XML configuration directory: \"{:s}\".'.format(configDir,fastConfigDir)) 
            Beckhoff.__mode = 'Default'    
        elif mode == 'ReadOnly':
            Beckhoff.__logger.info('Logger created.')    
            Beckhoff.__logger.info('Software initialized as Read Only mode.')        
            Beckhoff.__bp = BeckhoffParser(configDir, fastConfigDir)
            Beckhoff.__logger.info('XML parser initialized. XML configuration directory: \"{:s}\". Fast XML configuration directory: \"{:s}\".'.format(configDir,fastConfigDir)) 
            Beckhoff.__mode = mode
            
    @staticmethod
    def  set_io_map_file(io_map_path: str) -> None:
        '''
            Sets the file to write all received buffer data or open the data previously stored in case the Beckhoff mode is 'ReadOnly'.
            :param io_map_path: path to 
            :type: str
            :return: returns nothing
        '''        
        Beckhoff.__io_map_path = io_map_path
        if Beckhoff.__mode == 'ReadOnly':
            Beckhoff.__io_map_file= open(Beckhoff.__io_map_path,'r+')
            # Read used devices from file
            Beckhoff.__file_devices = []
            if Beckhoff.__io_map_file.readline() == '--Devices--Begin\n':                     
                for line in Beckhoff.__io_map_file:
                    if line == '--Devices--End\n': 
                        break # Processing done for now.
                    substr = line.split('\t')
                    device = {}
                    device.update({'name':substr[0]})
                    device.update({'index':int(substr[1])})
                    device.update({'revision':int(substr[2],16)})
                    input_prop = {}
                    input_prop.update({'start':int(substr[3])})
                    input_prop.update({'end':int(substr[4])})
                    input_prop.update({'size':int(substr[5])})
                    input_prop.update({'bitStart':int(substr[6])})
                    input_prop.update({'bitLen':int(substr[7])})
                    device.update({'input':input_prop})
                    output_prop = {}
                    output_prop.update({'start':int(substr[8])})
                    output_prop.update({'end':int(substr[9])})
                    output_prop.update({'size':int(substr[10])})
                    output_prop.update({'bitStart':int(substr[11])})
                    output_prop.update({'bitLen':int(substr[12])})
                    device.update({'output':output_prop})
                    Beckhoff.__file_devices.append(device)
            Beckhoff.__io_map_file.close()
        elif Beckhoff.__mode == 'Default':
            Beckhoff.__io_map_file = open(Beckhoff.__io_map_path,'w+',newline="\n")
            Beckhoff.__io_map_file.write('--Devices--Begin\n')            
            for device in Beckhoff.getModules():
                Beckhoff.__io_map_file.write(device['name'] + '\t')
                Beckhoff.__io_map_file.write(str(device['index']) + '\t')
                Beckhoff.__io_map_file.write('{:08x}'.format(device['revision']) + '\t')          
                Beckhoff.__io_map_file.write(str(device['input']['start']) + '\t') 
                Beckhoff.__io_map_file.write(str(device['input']['end']) + '\t') 
                Beckhoff.__io_map_file.write(str(device['input']['size']) + '\t')
                Beckhoff.__io_map_file.write(str(device['input']['bitStart']) + '\t')     
                Beckhoff.__io_map_file.write(str(device['input']['bitLen']) + '\t')               
                Beckhoff.__io_map_file.write(str(device['output']['start']) + '\t') 
                Beckhoff.__io_map_file.write(str(device['output']['end']) + '\t') 
                Beckhoff.__io_map_file.write(str(device['output']['size']) + '\t')
                Beckhoff.__io_map_file.write(str(device['output']['bitStart']) + '\t')
                Beckhoff.__io_map_file.write(str(device['output']['bitLen']) + '\t') 
                Beckhoff.__io_map_file.write('\n')
            Beckhoff.__io_map_file.write('--Devices--End\n')
            Beckhoff.__io_map_file.close()
        else:
            raise UnknownError('Beckhoff mode not defined/implemented. Check syntax')

    @staticmethod
    def getModules() -> list:
        '''
            Gets all beckhoff modules on the system
            :param: None
            
            :return: returns every beckhoff module information
            :rtype: dict
        '''
        
        if Beckhoff.__mode == 'ReadOnly':
            return Beckhoff.__file_devices
        return Beckhoff.__soem.getDevices()
    
    @staticmethod
    def printModules() -> None:
        '''
            Prints all discovered modules in the user setup
            
            :param: none
            :returns: returns nothing
        '''
        module = Beckhoff.getModules()
        for dev in module:
            print('{:03d}:{:s}'.format(dev['index'],dev['name']))
    
    def permitted(self, flags: dict or str) -> bool:
        '''
            Check if provided flag has permission for the desired operation
            :param flags: flags values such as read only or read-write
            :type: str or dict
            
            :return: returns if user has permission for the desired operation or not
            :rtype: bool
            
            :raises KeyError: If 'Access' key is not found in dictionary
            :raises TypeError: If flags is not a string or dict
        '''
        old_state = self.soem.getState()
        if isinstance(flags, str):
            if flags == Access.ro:
                return False
        elif isinstance(flags, dict):
            if '@WriteRestrictions' in flags:
                if flags['@WriteRestrictions'] == 'PreOP':
                    if old_state == EthercatSlaveStates.EC_STATE_OPERATIONAL:
                        return False
                    elif old_state == EthercatSlaveStates.EC_STATE_PRE_OP:
                        pass
                    else:
                        return False
                if flags['#text'] == Access.ro:
                    return False
            if 'Access' in flags:
                if flags['Access'] == Access.ro:
                    return False
            else:
                raise KeyError('Unknown Key. Possible reason: XML parser problem.')
        else:
            raise TypeError("was expecting <class \'str\'> or <class \'dict\'>.")
        
        return True
    
    def setParameter(self, parameterName: str, subParametervalue: str ="SubIndex 000", value: str="") ->str:
        ''' 
            Sets the beckhoff provided parameter
            Based from thopiekar.
            :param parameterName: Specific parameter in the beckhoff module profile provided by its XML configuration file
            :type: str
            
            :param subParametervalue: Sub parameter in the beckhoff module profile by its XML configuration file
            :type: str
            
            :param value: Value to sent with the parameter. If no value is set, default one will be sent
            :type: str
            
            :return: returns parameter value stored in beckhoff module
            :rtype: str
            
            :raises TypeError: If provided parameterName is not string
            :raises TypeError: If provided value is not a string
            :raises KeyError: If 'Parameter key is not in dictionary.
            :raises AccessDeniedError: User does not have permission to set parameter. 
            :raises RuntimeError: Parameter was incorretly set.
        '''        

        # Defensive programming  - Pre-conditions
        if not isinstance(parameterName, str):
            raise TypeError("Argument type error. Was expecting <class 'str'>.")
        if not isinstance(value, str):
            raise TypeError("Argument type error. Was expecting <class 'str'>.")

        # Check if parameter key exists in Beckhoff module profile
        if not 'Parameter' in self.profile:
            raise KeyError("\"Parameter\" Key is not in dictionary.")
        
        # Check if the desired parameter name exists in the parameter dictionary
        if not parameterName in self.profile['Parameter'].keys():
            Beckhoff.__logger.error('({:03d},{:s}) Unknown Parameter \"{:s}\" \"{:s}\"'.format(self.index,self.name,parameterName,subParametervalue))      
            raise KeyError("Provided parameter does not exit.")
        
        register_index, register_subindex, dataType, bitSize, flags, value = self.__bp.read_parameter(self.profile, parameterName, subParametervalue, value)
        if not self.permitted(flags):
            raise AccessDeniedError("User does not have access to write parameter or system state is not allowed ")
       
        val_map = ctypes.create_string_buffer(1 * (bitSize % 8 != 0) + int(bitSize / 8))
        
        if value.startswith('#x'):
            val_map.raw = int(value[2:], 16).to_bytes(len(val_map), 'little')
            value = value.replace('#x', '')
            tmpVal = int(value, 16)
        else:
            val_map.raw = int(value, 10).to_bytes(len(val_map), 'little')
            tmpVal = int(value, 10)

        tries = 1
        max_tries = 3
        current_value = self.getParameter(parameterName, subParametervalue, 'write')
        
        if (current_value == tmpVal):
            Beckhoff.__logger.warning('({:03d},{:s}) Impossible setting \"{:s}\" \"{:s}\" because new value \"{:s}\" was already loaded into the module.'.format(self.index,self.name,parameterName,subParametervalue,value))      
            return str(current_value)
        
        while tries <= max_tries and current_value != tmpVal:
            Beckhoff.__soem.writeRegister(self.index, register_index, register_subindex, val_map)          
            '''
            print("Setting state into EC_STATE_OPERATIONAL to apply parameter change!")
            self.soem.setStateAutomatically(EthercatSlaveStates.EC_STATE_OPERATIONAL)
            # Going back to the old state
            if old_state in EthercatSlaveStates.enum2string.keys():
                print("Setting state into the same state, like before changing the parameter: {}".format(EthercatSlaveStates.enum2string[old_state]))
            else:
                print("Setting state into the same state, which was a unknown one(!).".format(EthercatSlaveStates.enum2string[old_state]))
            #self.soem.setStateManually(pysoem.pysoem.EthercatSlaveStates.EC_STATE_INIT)
            self.soem.setStateAutomatically(old_state)
            '''            
            current_value = self.getParameter(parameterName, subParametervalue, 'write')
            tries += 1
        
        if current_value != tmpVal:
            Beckhoff.__logger.error('({:03d},{:s}) Parameter \"{:s}\" \"{:s}\" was not correctly setted.'.format(self.index,self.name,parameterName,subParametervalue,value))      
            raise RuntimeError("Parameter was not set corretly. Possible cause: user entered an invalid value.")
        
        Beckhoff.__logger.info('({:03d},{:s}) Parameter \"{:s}\" \"{:s}\" setted to \"{:s}"\ correclty.'.format(self.index,self.name,parameterName,subParametervalue,value))      
        return str(current_value)

    def getParameter(self, parameterName, subParameterName='SubIndex 000', cmd='read') -> int or bytes :
        ''' 
            Gets the beckhoff value using the providade parameter name
            Based from thopiekar.
            :param parameterName: Specific parameter in the beckhoff module profile provided by its XML configuration file
            :type: str
            
            :param subParametervalue: Sub parameter in the beckhoff module profile by its XML configuration file
            :type: str
            
            :param cmd: command to only be used inside beckhoff module. returns bytes if cmd is 'read' and datatype is STR otherwise int is returned.
            :type: str
            
            :return: value from desired parameter.
            :rtype: bytes or int
            
            :raises TypeError: If provided parameterName is not string
        '''
        
        if not isinstance(parameterName, str):
            raise TypeError("Argument type error. Was expecting <class 'str'>.")
        
        if self.profile == None:
            return None
        
        if not parameterName in self.profile["Parameter"].keys():
            Beckhoff.__logger.warning('({:03d},{:s}) Parameter \"{:s}\" \"{:s}\" does not exist.'.format(self.index,self.name,parameterName,subParameterName))      
            return None
        
        register_index, register_subindex, dataType, bitSize, flags, value = self.__bp.read_parameter(self.profile, parameterName, subParameterName)
                
        val_map = ctypes.create_string_buffer(1 * (bitSize % 8 != 0) + int(bitSize / 8))
        Beckhoff.__soem.readRegister(self.index, register_index, register_subindex, val_map)
        
        # If gerParameter is only intended to read
        if cmd == 'read':
            Beckhoff.__logger.info('({:03d},{:s}) Parameter \"{:s}\" \"{:s}\" value is \"{:s}\".'.format(self.index,self.name,parameterName,subParameterName,str(val_map.raw)))      
            if dataType.startswith("STR"):
                return val_map.raw
        Beckhoff.__logger.info('({:03d},{:s}) Parameter \"{:s}\" \"{:s}\" value is \"{:d}\".'.format(self.index,self.name,parameterName,subParameterName,int.from_bytes(val_map.raw, 'little')))      
        return int.from_bytes(val_map.raw, 'little')
          
    def checkSyncMode(self,cmd: str) -> int:
        '''
            Gets the sync mode so that can read the default TxPDO RxPDO. At this moment DC is not supported/tested.
            :param cmd: received synchronization mode.
            :type: int
            
            :return: returns the syncronization mode string which is compatible with the XML file
            :rtype: str
        '''
        val = None
        syncInput = {    0  : '0',
                         1  : '3', # 1 is SM3
                         2  : "DC - SYNC0",
                         3  : "DC-SYNC1",
                        34  : '2', # 2 is SM 2
                      None  : '99' }
        
        syncOutput ={    0  : '0',
                         1  : '2', # 1 is SM2
                         2  : "DC - SYNC0",
                         3  : "DC-SYNC1",
                      None  : "99" }
        
        if cmd == 'output':
            if(Beckhoff.__mode == 'ReadOnly'):
                Beckhoff.__io_map_file= open(Beckhoff.__io_map_path,'r+')                    
                for line in Beckhoff.__io_map_file:
                    if line.startswith('SM'):
                        strsplit = line.split('\t')
                        if self.index == int(strsplit[1]) and strsplit[2] == 'o':
                            val = strsplit[3]
                            if val == None:
                                raise UnknownError('Invalid file for ReadOnly mode')    
                            break
                     
                Beckhoff.__io_map_file.close()
                return val                    
            else:
                val = self.getParameter('SM output parameter','Sync mode')
                if  isinstance(Beckhoff.__io_map_path,str):
                    Beckhoff.__io_map_file= open(Beckhoff.__io_map_path,'a')
                    Beckhoff.__io_map_file.write('SM\t'+str(self.index)+'\to\t'+str(syncInput[val])+'\t\n')
                    Beckhoff.__io_map_file.close()
            return syncOutput[val]
        elif cmd == 'input':
            if(Beckhoff.__mode == 'ReadOnly'):
                Beckhoff.__io_map_file= open(Beckhoff.__io_map_path,'r+')
                for line in Beckhoff.__io_map_file:
                    if line.startswith('SM'):
                        strsplit = line.split('\t')
                        if self.index == int(strsplit[1]) and strsplit[2] == 'i':
                            val = strsplit[3]
                            if val == None:
                                raise UnknownError('Invalid file for ReadOnly mode') 
                            break          
                Beckhoff.__io_map_file.close() 
                return val                                 
            else:
                val = self.getParameter('SM input parameter','Sync mode')
                if  isinstance(Beckhoff.__io_map_path,str):
                    Beckhoff.__io_map_file= open(Beckhoff.__io_map_path,'a')
                    Beckhoff.__io_map_file.write('SM\t'+str(self.index)+'\ti\t'+str(syncInput[val])+'\t\n')
                    Beckhoff.__io_map_file.close()
                return syncInput[val]
   
    def assignInputs(self,manual_sm: str = ""):
        '''
            Assigns the inputs using XML file mapping to easily access data using commands.
            :param: None
            :return: returns nothing
        '''
        if manual_sm == "":
            self.inputs_map = self.__bp.read_Mapping(self.configs,'TxPdo', self.checkSyncMode('input'))
        else:
            self.inputs_map = self.__bp.read_Mapping(self.configs,'TxPdo', manual_sm)

    def assignOutputs(self,manual_sm: str = ""):
        '''
            Assigns the outputs using the XML file mapping to easily access data using commands.
            :param: None
            :return: returns nothing
        '''
        if manual_sm == "":
            self.output_map = self.__bp.read_Mapping(self.configs,'RxPdo', self.checkSyncMode('output'))
        else:
            self.output_map = self.__bp.read_Mapping(self.configs,'RxPdo', manual_sm)

    def assignPDO(self,manual_smIn:str = "", manual_smOut: str = "") -> None:
        '''
            Assigns inputs and outputs
            :param: None
            :return: returns nothing
        '''
        self.assignInputs(manual_smIn)
        self.assignOutputs(manual_smOut)

    def checkSettings(self) -> list:
        '''
            Returns all parameters stored in beckhoff module
            :param: None
            :return: returns a list with commands and correspondent data. each row is composed by a list of lenght 3. This is composed by first parameter command, second parameter command and third is the value.
            :rtype: list
        '''
        cmd = Beckhoff.__bp.get_all_parameters(self.profile)
        sett = []
        for i in range(len(cmd)):
            if cmd[i][1] == "":
                val = self.getParameter(cmd[i][0])
            else:
                val = self.getParameter(cmd[i][0], cmd[i][1])
            sett.append([cmd[i][0],cmd[i][1],val])
        return sett

    @staticmethod
    def get_io_map() -> bytes:
        '''
            Gets the raw data from all beckhoff modules
            :param: None
            :return: returns beckhoff modules received raw data.
            :rtype: bytes
        '''
        return Beckhoff.__data;

    @staticmethod
    def sendOnly() -> None:
        '''
            This function is only used if the user is not expecting to receive nothing from the modules
            :param: None
            :return: returns nothing
            :rtype: bytes
        '''
        Beckhoff.__soem.send_processdata()

if __name__ == "__main__":

    iface = "enp0s20f0u4u1i5"  # Used Ethernet Interface
    freq = 100  # Sampling Frequency
    
    xmlConfigPath = "../BeckXML/"
    dicConfigPath = "../BeckXML/PyMotorConfig/"
    
    module = []
    
    dev_required = ("EK1100", "EL4002", "EL5032", "EL7332", "EL3104",
                    "EL3102", "EL2521", "EL3318", "EL9508", "EL9505",
                    "EL2124", "EL2124", "EL2124", "EL1124")
    
    # Initialize Beckhoff interface and Beckhoff XML configuration files location
    Beckhoff.init(iface, xmlConfigPath, dicConfigPath, logging.DEBUG)
    # Creating wanted Beckhoff modules entries
    for x in range(len(dev_required)):    
        module.append(Beckhoff(dev_required[x], x + 1))
    
    # Search if wanted configuration matches the user configuration
    if(not Beckhoff.found(module)):
        print("Beckhoff modules not found.")
        sys.exit()
        
    # Setting up modules
    for device in Beckhoff.getModules():
        module[device["index"] - 1].setup(device)

    print(module[4].checkSettings())
    
    # Prints a table of the bits offset in memory    
    Beckhoff.printOffsetAssignment(module)
    
    # Checking if setParameter and getParameter functions are working accordingly
    val = module[7].getParameter("Device name")
    print("[Beckhoff] main() - Received value from \"Device name\" parameter : {:s}".format(str(val)))
    module[7].setParameter('TC Settings Ch.1', 'Filter settings', "#x0008")
    
    val = module[7].getParameter('TC Settings Ch.1', 'Filter settings')
    print("[Beckhoff] main() - Received value from \"TC Settings Ch.1 - Filter settings\" parameter : {:s}".format(str(val)))

    val = module[3].setParameter('DCM Motor Settings Ch.1', 'Maximal current', '1000')

    val = module[3].getParameter('DCM Motor Settings Ch.1', 'Maximal current')
    print("[Beckhoff] main() - Received value from \"DCM Motor Settings Ch.1 - Maximal current\" parameter : {:s}".format(str(val)))
    
    val = module[3].getParameter('DCM Inputs Ch.1', 'Ready to enable')
    print("[Beckhoff] main() - Received value from \"DCM Inputs Ch.1 - Ready to enable\" parameter : {:s}".format(str(val)))
    
    val = module[3].getParameter('DCM Inputs Ch.1', 'Ready')
    print("[Beckhoff] main() - Received value from \"DCM Inputs Ch.1 - Ready\" parameter : {:s}".format(str(val)))
    
    val = module[3].getParameter('DCM Inputs Ch.1', 'Warning')
    print("[Beckhoff] main() - Received value from \"DCM Inputs Ch.1 - Warning\" parameter : {:s}".format(str(val)))
    
    val = module[3].getParameter('DCM Inputs Ch.1', 'Error')
    print("[Beckhoff] main() - Received value from \"DCM Inputs Ch.1 - Error\" parameter : {:s}".format(str(val)))
    
    val = module[3].getParameter('DCM Outputs Ch.1', 'Enable')
    print("[Beckhoff] main() - Received value from \"DCM Outputs Ch.1 - Enable\" parameter : {:s}".format(str(val)))
    
    val = module[3].getParameter('DCM Inputs Ch.1', 'Error')
    print("[Beckhoff] main() - Received value from \"DCM Inputs Ch.1 - Error\" parameter : {:s}".format(str(val)))

    module[3].setParameter('DCM Features Ch.1', 'Operation mode', '1')
    val = module[3].getParameter('DCM Features Ch.1', 'Operation mode')
    print("[Beckhoff] main() - Received value from \"DCM Features Ch.1 - Operation mode\" parameter : {:s}".format(str(val)))

    module[3].setParameter('DCM Features Ch.2', 'Operation mode', '1')
    val = module[3].getParameter('DCM Features Ch.2', 'Operation mode')
    print("[Beckhoff] main() - Received value from \"DCM Features Ch.2 - Operation mode\" parameter : {:s}".format(str(val)))

    module[6].setParameter('Feature bits', 'Operating mode', '1')
    val = module[6].getParameter('Feature bits', 'Operating mode')
    print("[Beckhoff] main() - Received value from \"Feature bits - Operating mode\" parameter : {:s}".format(str(val)))

    val = module[6].getParameter('PTO Settings', 'Ramp function active')
    print("[Beckhoff] main() - Received value from \"TO Settings - Ramp function active\" parameter : {:s}".format(str(val)))

    module[6].setParameter('Volatile settings', 'Target counter state',"0")
    val = module[6].getParameter('Volatile settings', 'Target counter state')
    print("[Beckhoff] main() - Received value from \"Volatile settings - Target counter state\" parameter : {:s}".format(str(val)))

    module[6].setParameter('User settings', 'Base frequency 1','50000')
    val = module[6].getParameter('User settings', 'Base frequency 1')
    print("[Beckhoff] main() - Received value from \"User Settings - Base frequency 1\" parameter : {:s}".format(str(val)))

    val = module[6].getParameter('Inputs','Status')
    print("[Beckhoff] main() - Received value from \"ENC Inputs\" parameter : {:s}".format(str(val)))

    val = module[2].getParameter('Restore default parameters')
    print("[Beckhoff] main() - Received value from \"User Settings - Base frequency 1\" parameter : {:s}".format(str(val)))

    val = module[5].getParameter('AI Advanced settings Ch.1')
    print("[Beckhoff] main() - Received value from \"User Settings - Base frequency 1\" parameter : {:s}".format(str(val)))

    val = module[1].getParameter('AO settings Ch.1','Enable user scale')
    print("[Beckhoff] main() - Received value from \"AO settings Ch.1 - Enable user scale\" parameter : {:s}".format(str(val)))

    val = module[1].getParameter('AO settings Ch.1','Presentation')
    print("[Beckhoff] main() - Received value from \"AO settings Ch.1 - Presentation\" parameter : {:s}".format(str(val)))

    val = module[1].getParameter('AO settings Ch.1','Enable user calibration')
    print("[Beckhoff] main() - Received value from \"AO settings Ch.1 - Enable user calibration\" parameter : {:s}".format(str(val)))

    val = module[1].getParameter('AO settings Ch.1','Enable vendor calibration')
    print("[Beckhoff] main() - Received value from \"AO settings Ch.1 - Enable vendor calibration\" parameter : {:s}".format(str(val)))

    val = module[1].getParameter('AO settings Ch.1','Default output')
    print("[Beckhoff] main() - Received value from \"AO settings Ch.1 - Default output\" parameter : {:s}".format(str(val)))
    val = module[1].setParameter('AO settings Ch.1','Default output','16000')

    val = module[1].getParameter('AO internal data Ch.1','DAC raw value')
    print("[Beckhoff] main() - Received value from \"AO internal data Ch.1 - DAC raw value\" parameter : {:s}".format(str(val)))
   
    val = module[1].getParameter('AO vendor data Ch.1','Calibration offset')
    print("[Beckhoff] main() - Received value from \"AO vendor data Ch.1 - Calibration offset\" parameter : {:s}".format(str(val)))
   
    val = module[1].getParameter('AO vendor data Ch.1','Calibration gain')
    print("[Beckhoff] main() - Received value from \"AO vendor data Ch.1 - Calibration gain\" parameter : {:s}".format(str(val)))
   
    val = module[1].getParameter('AO settings Ch.1','Gain')
    print("[Beckhoff] main() - Received value from \"AO settings Ch.1 - Gain\" parameter : {:s}".format(str(val)))

    val = module[1].getParameter('AO settings Ch.1','Offset')
    print("[Beckhoff] main() - Received value from \"AO settings Ch.1 - Offset\" parameter : {:s}".format(str(val)))

    val = module[1].getParameter('AO settings Ch.1','User calibration gain')
    print("[Beckhoff] main() - Received value from \"AO settings Ch.1 - User calibration gain\" parameter : {:s}".format(str(val)))

    val = module[1].getParameter('AO settings Ch.1','User calibration offset')
    print("[Beckhoff] main() - Received value from \"AO settings Ch.1 - User calibration offset\" parameter : {:s}".format(str(val)))   
   
    val = module[1].getParameter('AO outputs Ch.1','Analog output')
    print("[Beckhoff] main() - Received value from \"AO outputs Ch.1 - Analog output\" parameter : {:s}".format(str(val)))
   

    # Assign PDO
    for device in Beckhoff.getModules():
        module[device["index"] - 1].assignPDO()

    # print(module[6].checkSyncMode("input"))

    #module[7].assignInputs()
    #module[2].assignInputs()
    module[11].assignOutputs()
    module[10].assignOutputs()
    
    var = int(0)
    # buffer1 = bytes(b'\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x01\x00\x00\x00'

    # Start measuring
    Beckhoff.start(freq)
    tmp = 0;

    for n in range(10000):
        Beckhoff.updateInput()  # Updates All Beckhoffs data buffer
        out = "%d " % (Beckhoff.getTimeStamp())  # Get timestamp from updated data buffer
        
        # Write Outputs to desired module                                                                       
        if(Beckhoff.dataSent() > 0):
#           module[3].setOutput('1',"DCM Control Channel 1", "Control__Enable")
            module[10].setOutput('1',"Channel 1")
            module[10].setOutput('1',"Channel 2")
            module[10].setOutput('1',"Channel 3")
            module[10].setOutput('1',"Channel 4")
            module[10].updateOutput()
            
            module[11].setOutput('1',"Channel 1")
            module[11].setOutput('1',"Channel 2")
            module[11].setOutput('1',"Channel 3")
            module[11].setOutput('1',"Channel 4")
            module[11].updateOutput()

            module[12].setOutput('1',"Channel 1")
            module[12].setOutput('1',"Channel 2")
            module[12].setOutput('1',"Channel 3")
            module[12].setOutput('1',"Channel 4")
            module[12].updateOutput()

            
            module[3].setOutput('1',"DCM Control Channel 1", "Control__Enable")
            module[3].setOutput('1',"DCM Control Channel 1")
            module[3].setOutput('#x0010',"DCM Velocity Channel 1")
            module[3].setOutput('1',"DCM Control Channel 2", "Control__Enable")
            module[3].setOutput('1',"DCM Control Channel 2")
            module[3].setOutput('#x0010',"DCM Velocity Channel 2")
            
            #module[3].setOutput('1',"DCM Control Channel 1"
            #module[3].setOutput('#x7777',"DCM Velocity Channel 1")

           # module[3].setOutput('1',"DCM Control Channel 2")
           #module[3].setOutput('7172',"DCM Velocity Channel 2")
           # tmp = 1 - tmp
           # module[12].setOutput(1-tmp,"Channel 2")
            
        # Read Inputs from every module and print it
        
        #print(binascii.hexlify(module[2].getInput("FB Inputs Channel 1","Position")).decode('ascii'))
        #print(binascii.hexlify(module[2].getInput("FB Inputs Channel 1")).decode('ascii'))
        out = "TS: %d " % (Beckhoff.getTimeStamp())  # Get timestamp from updated data buffer
        #out += '| Temperature: '8,'Status Uo','Power OK'
        out += '\tPower = {:d}'.format(int(binascii.hexlify(module[8].getInput("Status Uo",'Power OK')),16))
        print('\tRAW = {:s}'.format(binascii.hexlify(module[6].getInput()).decode('ascii')))

        #out += '\tTC2 = {:d}'.format(int(binascii.hexlify(module[7].getInput("TC Channel 2",'Value')),16))
        ##out += '\tTC3 = {:d}'.format(int(binascii.hexlify(module[7].getInput("TC Channel 3",'Value')),16))
        #out += ' |'
        #print(out)

        print(out)    
    Beckhoff.stop()
    
        
        
