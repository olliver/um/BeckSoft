# BeckSoft v0.1.0
This project aims to develop an open source interface to interact with [Beckhoff](https://beckhoff.com) modules. It was build on top of [SOEM](https://github.com/OpenEtherCATsociety/SOEM) and [PySOEM](https://github.com/Ultimaker/pysoem).
**BeckSoft** provides an easy way to interact with Beckhoff modules by getting its data from configuration files. This allows to the user to get modules data by typing an appropriate instruction.

## Getting Started


### Pre-requisites
For this project to work it is necessary to build 2 packages. SOEM, which is a package written in C to interact with EtherCAT protocol. PySOEM works as a C to Python translator and provides an optimized way in using SOEM.

The main objective in SOEM is extracting libsoem.a into /usr/local/lib to satisfy PySOEM requirement.
```bash
git clone https://github.com/OpenEtherCATsociety/SOEM.git
cd SOEM
mkdir build && cd build
cmake -DCMAKE_INSTALL_PREFIX="/usr/local" -DCMAKE_C_FLAGS="-Wall -Wextra -Werror -fPIC" ..
make 
sudo make install
```
On a separate folder, build PySOEM. This will install the build python package into /ust/lib/<python_version>/site-packages.
```bash
git clone -b outPySoem https://github.com/Ultimaker/pysoem.git
cd pysoem
mkdir build && cd build
cmake -DCMAKE_POSITION_INDEPENDENT_CODE=ON -DCMAKE_INSTALL_PREFIX:PATH=/usr ..
make
sudo make install
```
Installing necessary python modules such as xmltodict. xmlTo
```
pip install xmltodict pickle
```

### Installing

To install ***BeckSoft*** please perform the following steps.

```bash
pip install git+https://github.com/Ultimaker/BeckSoft.git
```
or, in case you need to update the module:
```bash
pip install --upgrade git+https://github.com/Ultimaker/BeckSoft.git
```
That's it, now you are ready to use ***BeckSoft*** Software. You just need to import the module in your python script as:

```
import beckSoft
```
### Example
To test the setup, to check if everything is working correctly, you can use the following code.
First, imports:
```python
from beckSoft.beckhoff import Beckhoff
import logging
import binascii
```
Creating an empty module list:
```python
module = []
```
Ethernet used interface name to communicate with the Beckhoff modules, in this case was *enp0s31f6*:
```python
iface = "enp0s31f6"
```
Thread Sending/Receiving frequency:
```python
freq = 100
```
Location for the XML files with the Beckhoff modules information:  
```python
xmlConfigPath = "../BeckXML/"
```
Location to read or store the Beckhoff modules information. After first run, the obtained Beckhoff modules information will be stored and on a second run this information will be automatically loaded. The advantage of this implementation is the fast startup of ***BeckSoft***:
```python
dicConfigPath = "../BeckXML/PyMotorConfig/"
```
Initialize Beckhoff interface, Beckhoff XML configuration files at provided location and Debug level:
```python
Beckhoff.init(iface, xmlConfigPath, dicConfigPath, logging.DEBUG)
```
Initializing Beckhoff modules:
```python
for device in Beckhoff.getModules():
    module.append(Beckhoff(device['name'],device['index']))
```
Print all modules discovered in your system:
```python
print('Modules discovered')
Beckhoff.printModules()
```
Setting up Beckhoff modules. This will obtain the module configuration from XML and map the memory for the received/send data.    
```python
for device in Beckhoff.getModules():
    module[device['index']-1].setup(device)
```
Prints the addresses from IO_Map assigned to each module.
```python
Beckhoff.printOffsetAssignment(module)
```

## Built With

* [Eclipse](http://www.eclipse.org/) - IDE
* [PyDev](http://www.pydev.org) - Python plugin for eclipse



## Versioning
0.1.0 Actual version
    Changes: 
        - Speed improvements by storing the mask when it is created for the first time.
        - Bugs fixed in Parser
        - Possible to obtain all possible parameters inside the beckhoff modules and its values.
        Offline mode added. Now it is possible to read data from files for later to post process without the need of having the setup.
        - Function to send packet without expecting to receive one added.
        - Documentation errors fixed.

0.0.0 Project Added and ready to be used

## Authors

* **Kevin Filipe** - [cosmicKev](https://github.com/cosmicKev)

## Acknowledgments
[PySOEM](https://github.com/Ultimaker/pysoem)
[SOEM](https://github.com/OpenEtherCATsociety/SOEM) 
