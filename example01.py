'''
    example01.py - Simple 
'''

from beckSoft.beckhoff import Beckhoff
import logging
import binascii
module = []
# Used Ethernet Interface
iface = "enp0s20f0u4u1i5:"
# Sampling Frequency
freq = 100  
# XML Configuration Path  
xmlConfigPath = "../BeckXML/"
# Fast XML Configuration Path
dicConfigPath = "../BeckXML/PyMotorConfig/"
# Initialize Beckhoff interface, Beckhoff XML configuration files at provided location and Debug level
Beckhoff.init(iface, xmlConfigPath, dicConfigPath, logging.DEBUG)
    
# Creating modules
for device in Beckhoff.getModules():
    module.append(Beckhoff(device['name'],device['index']))

# Print all modules discovered in your system
print('Modules discovered')
Beckhoff.printModules()

# Setting up modules    
for device in Beckhoff.getModules():
    module[device['index']-1].setup(device)

# Prints the addresses from IO_Map assigned to each module.
Beckhoff.printOffsetAssignment(module)
