'''
    example04.py - Simple Counter.
'''
from beckSoft.beckhoff import Beckhoff
import logging
import binascii
from time import sleep

module = []
# Used Ethernet Interface
iface = "enp0s20f0u4u1i5"
# Sampling Frequency
freq = 50  
# XML Configuration Path  
xmlConfigPath = "../BeckXML/"
# Fast XML Configuration Path
dicConfigPath = "../BeckXML/PyMotorConfig/"
# Output module Index - IMPORTANT
idx = 12 # Put the number of your GPIO index. In my setup, Beckhoff module with GPIO capabilities was number 12

# Initialize Beckhoff interface, Beckhoff XML configuration files at provided location and Debug level
Beckhoff.init(iface, xmlConfigPath, dicConfigPath, logging.DEBUG)
    
# Creating modules
for device in Beckhoff.getModules():
    module.append(Beckhoff(device['name'],device['index']))
    
# Setting up modules    
for device in Beckhoff.getModules():
    module[device['index']-1].setup(device)
    
 # Start
Beckhoff.start()   
cnt = 0    
for n in range(1000):
    Beckhoff.updateInput()  # Updates All Beckhoffs data buffer
    if Beckhoff.dataSent() > 0:
        cnt = cnt + 1
        module[idx].setOutput(cnt) # Note: Some modules only have one PDO mapping. If so, there is no need to declare the mapping. 
        module[idx].updateOutput()  # Updates specified beckhoff output
        sleep(0.5)
Beckhoff.stop()
