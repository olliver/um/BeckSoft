'''
    example02.py - Data aquisition and storage so that later can be used for post-processing
'''
from beckSoft.beckhoff import Beckhoff
import logging
import binascii
module = []
# Used Ethernet Interface
iface = "enp2s0"
# Sampling Frequency
freq = 100  
# XML Configuration Path  
xmlConfigPath = "../BeckXML/"
# Fast XML Configuration Path
dicConfigPath = "../BeckXML/PyMotorConfig/"
# Initialize Beckhoff interface, Beckhoff XML configuration files at provided location and Debug level
Beckhoff.init(iface, xmlConfigPath, dicConfigPath, logging.DEBUG)

# Creating modules
for device in Beckhoff.getModules():
    module.append(Beckhoff(device['name'],device['index']))

# Print all modules discovered in your system
print('Modules discovered')
Beckhoff.printModules()

# Setting up modules    
for device in Beckhoff.getModules():
    module[device['index']-1].setup(device)

# Define path and file name and create file to store input data
Beckhoff.set_io_map_file("inputs.txt")    

# Prints the addresses from IO_Map assigned to each module.
Beckhoff.printOffsetAssignment(module)


# Assign PDO
for device in Beckhoff.getModules():
    module[device['index']-1].assignPDO()
    
# Start measuring
Beckhoff.start(freq)
tmp = 0;

# Acquiring Data from Beckhoff
for n in range(1000):
    Beckhoff.updateInput()  # Updates All Beckhoffs data buffer
Beckhoff.stop()
    
