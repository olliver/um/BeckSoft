'''
    Example03.py - Offline processing Beckhoff module
'''

# Post processing example
from beckSoft.beckhoff import Beckhoff
import logging
import binascii
module = []

# XML Configuration Path  
xmlConfigPath = "../BeckXML/"

# Fast XML Configuration Path
dicConfigPath = "../BeckXML/PyMotorConfig/"

# Initialize Beckhoff interface, Beckhoff XML configuration files at provided location and Debug level
Beckhoff.init(configDir=xmlConfigPath, fastConfigDir=dicConfigPath, mode='ReadOnly')

# Define path and file name to be read
Beckhoff.set_io_map_file("inputs.txt")

# Creating modules
for device in Beckhoff.getModules():
    module.append(Beckhoff(device['name'],device['index']))

# Print all modules discovered in your system
print('Modules discovered')
Beckhoff.printModules()

# Setting up modules    
for device in Beckhoff.getModules():
    module[device['index']-1].setup(device)

# Prints the addresses from IO_Map assigned to each module.
Beckhoff.printOffsetAssignment(module)

# Assign PDO
for device in Beckhoff.getModules():
    module[device['index']-1].assignPDO()

# Define path and file name and create file to store input data
Beckhoff.set_io_map_file("inputs.txt")    
    
# Start processing
Beckhoff.start()   
    
# Processing acquired data.
while(Beckhoff.updateInput()):
    out = "TS: %d " % (Beckhoff.getTimeStamp())  # Get timestamp from updated data buffer
    # Put reading statements here:
    # module[x].getInputs(.....)
    print(out)
Beckhoff.stop()